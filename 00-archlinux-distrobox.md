# Creating an Arch Linux Distrobox with `paru` installed

1. Create `/home/deck/Distroboxes/` for cleanliness (I will be assigning a home directory to the distrobox).

2. `cd` into `/home/deck/Distroboxes/`.

3. Run the following command to create an Arch Linux Distrobox. I have named mine `arch_exporter`.
```
distrobox create --name arch_exporter --image archlinux:latest --home ~/Distroboxes/arch_exporter
```
4. Enter into the newly created distrobox.
```
distrobox enter arch_exporter
```
4. Run `sudo pacman -Syyu`

5. Install `git` and `base-devel`.
```
sudo pacman -S git base-devel
```
6. Clone `paru` from the AUR.
```
git clone https://aur.archlinux.org/paru.git
```
7. `cd` into newly created `paru` directory and run `makepkg -si`

8. Run `paru` to test.

9. `exit` when done.
