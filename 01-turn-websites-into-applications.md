# Installing Websites as Native Applications
The idea is to use `nodejs-nativefier` to create a desktop application for any website.

### Prerequisites
- An `archlinux` distrobox with paru installed (![How-to](00-archlinux-distrobox.md)). While reading this guide, assume an already created distrobox named `arch_exporter`.

### Instructions

| # | Step | Command |
| ------ | ------ | ------ |
| 1 | Enter into the `archlinux` `distrobox`. | `distrobox enter arch_exporter` |
| 2 | Install `nodejs-nativefier`. | `paru -S nodejs-nativefier` |
| 3 | In a suitable directory (ex. `/home/deck/WebApps/`), run the `nativefier` command as shown here -> | `nativefier -p linux -a x64 -n "<App-Name>" "app-url" --tray --verbose --single-instance --disable-dev-tools` |
| 4 | `exit` from the distrobox, `cd` into `<App-Name>-linux-x64` and make the `<App-Name>` file executable. | `cd <App-Name>-linux-x64` and then `chmod +x <App-Name>` |
| 5 | Test the application. | `./<App-Name>` |
| 6 | Create a an empty file named `<App-Name>.desktop` file for the ability to double-click to open, as well as adding it as a non-steam game. Open it with `Kate` fill in -> | Use template given below |

#### .desktop file template
```
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=App-Name
Comment=Whatever You Like Here
Exec=/home/deck/WebApps/<App-Name>-linux-x64/<App-Name>
Icon=/home/deck/Pictures/Icons/app-name.png
Type=Application
Encoding=UTF-8
Terminal=false
Categories=None;
```

### Example - Reddit 
1. Assumptions: Currently in `arch_exporter` distrobox; Currently in `/home/deck/WebApps` directory; Icon for app has been downloaded to `/home/deck/Pictures/Icons`.

![1](img/01/1.png)

2. Run `nativefier -p linux -a x64 -n "Reddit" "https://www.reddit.com" --verbose --tray --disable-dev-tools --single-instance`.

![2](img/01/2.png)

3. `exit` `arch_exporter` distrobox.

![3](img/01/3.png)

4. `cd /home/deck/WebApps/Reddit-linux-x64`.

![4](img/01/4.png)

5. `chmod +x Reddit`.

![5](img/01/5.png)

6. Test application by running `./Reddit` in the `Reddit-linux-x64` directory.

![6](img/01/6.png)

7. Create an empty file named `Reddit.desktop` and add in the following contents using `Kate`.

![7](img/01/7.png)

```
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Reddit
Comment=Dive into Anything
Exec=/home/deck/WebApps/Reddit-linux-x64/Reddit
Icon=/home/deck/Pictures/Icons/reddit.png
Type=Application
Encoding=UTF-8
Terminal=false
Categories=None;
```
