# Emulate Android on Steam Deck

### Features
- Start Androidx86 (Bliss OS/Berry OS) emulator on the Steam Deck with one click. 
- No need to fiddle with the read-only filesystem.
- No Genymotion account required.
- No need to log into any service (Google, etc) unless required by some application.

### See Concerns at the end of this guide.


## Prerequisites
- `berry-os-9.glibc2.34-x86_64.AppImage` from ![yui0/berry-os](https://github.com/yui0/berry-os/releases/tag/2023-02-16)

## Setup Instructions
1. Start Desktop Mode. 

2. Navigate to the directory containing `berry-os-9.glibc2.34-x86_64.AppImage`

3. _Right Click_ -> _Open Terminal Here_

![2](img/02/2.png)

4. Make `berry-os-9.glibc2.34-x86_64.AppImage` executable.
```
chmod +x berry-os-9.glibc2.34-x86_64.AppImage
```
5. Run `berry-os-9.glibc2.34-x86_64.AppImage`
```
./berry-os-9.glibc2.34-x86_64.AppImage
```
6. A window will open with Android starting up. It will reach the lock screen.

![4](img/02/4.png)  ![5](img/02/5.png)

7. _Click_ + _Drag_ to unlock Android.

![6](img/02/6.png)

8. In Android, open Settings App -> Network & Internet -> Toggle WiFi on. 
   Once it searches for Networks, you will see one network named `VirtWifi`. 
   Click on it to connect to it, and then close settings.

![7](img/02/7.png)

9. Android has now been set up, and it is possible to install apps. Close the Android emulator for now.

10. \[Optional] Download an icon for the Android emulator for the shortcut that will be created for it in Step 14.

11. In a convenient directory, _Right Click_ -> _Create New_ -> _Empty File_. Name the file `android.desktop`.

12. In the same directory, _Right Click_ -> _Create New_ -> _Empty File_. Name the file `fullscreen_android.py`.

13. Open `fullscreen_android.py` with `Kate` and fill in the following lines of code.
```
#!/usr/bin/env python3
import subprocess
import time
import sys

app = sys.argv[1]
t = 0

subprocess.Popen(app)
while t < 10:
    if t == 5:
        subprocess.call(["xdotool", "key", "ctrl+alt+f"])
    time.sleep(0.5)
    t = t+1

```

14. Open `android.desktop` with `Kate` and fill in the following lines of text with the correct paths:
```
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=Android Pie
Comment=Berry OS 9 (Android 9; Bliss OS 11.4)
Exec=python3 /home/deck/Applications/Android/fullscreen_android.py /home/deck/VMs/Android/berry-os-9.glibc2.34-x86_64.AppImage
Icon=/home/deck/Pictures/Icons/android-pie.png
Type=Application
Encoding=UTF-8
Terminal=false
Categories=None;
```

15. Open _Steam_ -> _Add a Non-Steam Game_ -> _Browse_ -> Go into the directory with `android.desktop` and add it. It should now be available in Game Mode.

16. Switch to Game Mode and select the "Android Pie" entry.

17. Click on the **button with the controller icon in it** and Change the layout to "Gamepad"

![8](img/02/8.jpg)

18. Map _Right Track Pad Click_ as _Left Click_.

![9](img/02/9.jpg)

19. Map ![☰](https://www.fileformat.info/info/unicode/char/2630/index.htm) button to _Ctrl_ + _Alt_ + _Delete_. This button will allow you to shutdown the emulator. (Set _Ctrl_ as the main command, and _Alt_ and _Delete_ as sub commands in that order).

![10](img/02/10.jpg)

20. Map _Right Joystick_ as _Scrol Wheel Up_ and _Scrol Wheel Down_.

![11](img/02/11.jpg)

21. You can now start the emulator

### Demo Video (forgive the bad angle; I suck at this)

![12](img/02/demo_video.mp4)


### Data Location
- A file named `data.vmdk` is automatically created in `/home/deck/.local/share/berry-os/`. You have to delete it if you want to reset to a fresh installation of the emulator.

## Concerns and Issues

- This is a random file found on the internet. Exercise caution when logging into anything.
- `arm`/`arm64` applications install successfully, but crash on launch. Running the command `houdini --version` in `Android Terminal` app returns version `9.0.5c_y.51331`. I'm not skilled enough to fix this problem.
- The emulator's filesystem is only 20GB in size. 
- There is another version of berry-os in ![yui0/berry-os](https://github.com/yui0/berry-os) named ` berry-os-12-20230222.glibc2.34-x86_64.AppImage`. I refrained from using it as:
  - I am unable to install from any source other than the Aurora Store and the Play Store.
  - I am also unable to download files using the browser, which stopped me from sideloading .apk files.
  - The Settings app is buggy. It only shows the "Network & Internet" section.
  - I may not have the expertise to get it working.


## Credits
- ![yui0/berry-os](https://github.com/yui0/berry-os) for the AppImages
- ![Force Fullscreen](https://askubuntu.com/a/698663)
